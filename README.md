# PopDate

## Overview

PopDate is a simple framework-independent function to add a calendar to input fields.  

## Usage

At first, include the script and CSS styles (optionally) in your html page:

```html
<link href="popdate.css" rel="stylesheet" media="all" />
<script type="text/javascript" src="popdate.js"></script>
```

Then init the element after its loading:

```js
popDate(document.getElementById('myinput'));
```

You can use framework-independent construction to emulate the behavior similar to jQuery(document).ready() 

```js
(function() {
    function init() {
        var inputs = document.querySelectorAll('.date-input');
        for (var i = 0; i < inputs.length; i++) {
            popDate(inputs[i]);
        }
    }

    function onload() {
        try {
            document.addEventListener("DOMContentLoaded", init);
        } catch (err) {
            function timeoutLoader() {
                if (document.body == null) {
                    setTimeout(timeoutLoader, 300)
                } else {
                    init();
                }
            }
            timeoutLoader();
        }
    }
    onload();
})();
```

Same with jQuery:

```js
jQuery(document).ready(function(){
	jQuery('.date-input').each(function(){
		popDate(jQuery(this).get(0));
	});
});
```
Or you can create your own jQuery plugin:

```js
$.fn.popdate = function (options) {
	this.each(function () {
		popDate($(this).get(0), options);
	});  
}
/* after that you can use script like
$('input').popDate();
*/
```

## Options

For advanced date management you can run the function with object of settings:

```js
popDate(input, {
	param: value,
});

``` 

Available parameters: 

* **udate** _(mixed)_ - user's date, as default date popDate sets today date, use _integer_ to shift the month back or forward, (for instance use 1 to show next month or -2 to show the month before previous). You can use _string_ in format _mm.yyyy_ to set the specific month and year.
* **format** _(string)_ - format of the inserted date, available aliases are:
	- {D} - day as "2"
	- {DD} - day as "02"
	- {M} - month as "5"
	- {MM} - month as "05"
	- {MMM} - month as "jan"
	- {MMMM} - month as "January"
	- {MMMMM} - month as specified in _months2_ option
	- {YY} - year as "89"
	- {YYYY} - year  as "1989"
* **multiple** _(mixed)_ - as default the only one calendar object will be shown, but you can display as many calendars for the field as you want. For instance, you need to show the current and the next 2 months the same time. Use an _integer_ to set quantity of months (starts from current to next ones). Also you can use an _array_ to set calendars like in _udate_ option:

```js
multiple: [-1, 2, '04.1996'] 
/* this one will shows the previous,
 the next after the next months 
and April of 1996 */
```
* **closeOnChoose** _(boolean)_  - set _true_ if you want the calendar to stay shown after picking a date
* **days** _(array)_ - translation for day names, short names like "su", "mo" are recommended
* **months** _(array)_ - translation for months, used for  month header and format {MMM} and {MMMM}
* **months2** _(array)_ - translation for months which used in format {MMMMM} (useful for many languages)
* **sundayFirst** _(boolean)_ - set _false_ to make Monday starts first, do not forget to change days translations accordingly 
* **onChoose** _(function)_ - function executes on date picking
* **onLoad** _(function)_ - function executes on initialization 


### Callback functions

#### Traversing

Both functions _onChoose_ and _onLoad_ execute in **div.popdate-cal-wrapper** context, because it provides more useful abilities than **window** context. For example, you can use traversing relative to event target.

Structure of input after script initializaton:

```html
<div class="sample_container">
	<input name="non" type="text">
	<span class="popdate-cal-btn ></span>
	<span class="popdate-cal-wrapper">
		<div class="popdate-cal-layer">
			Some calendar tables...
		</div>
	</span>
</div>
```
You can easily find .sample_container using native js:

```js
function(){
	this // the .popdate-cal-wrapper of calendar that initiate the event
	.parentElement
	.style.fontFamily = 'monospace';
}
```
or using jQuery:

```js
$(this).parents('.sample_container').addClass('popdate-marked');
```

#### Arguments

##### onLoad

As you can show multiple calendars, you can use properties of each calendar. 

* _day_ - weekday number (depends on sundayFirst option starts with 0)
* _date_ - today's date
* _month_ - value of getMonth() method
* _monthName_  - name as assigned in translations
* _monthName2_  - name as assigned in translations (_months2_)
* _year_ - value of getFullYear method
* _nextMonth_ - object Date of the next month
* _prevMonth_ - object Date of the next month
* _prevMonthName_ and _nextMonthName_ - translations for these months
* _days_ - last day of the month date
* _firstday_ - number of the weekday of the fistday (depends on sundayFirst option starts with 0)
* _lastday_ - number of the weekday of the lastday (depends on sundayFirst option starts with 0)

```js
onLoad : function (cal1, cal2){
	/* for example the first calendar is the current month
	 and the second is the next one */
	alert("There's only " 
	+ (cal1.days - cal1.date) 
	+ " days left in "
	+ cal1.monthName
	+ "! Try the script in "
	+ cal2.monthName);
}
```

##### onChoose

This function uses only one argument that returns an object with all available formats of the date. List of properties:

* d, dd, m, mm, mmm, mmmm, mmmmm, yy, yyyy - parts of chosen date
* format  - the same string that inserts in input element

You can use it nice with traversing:

```html
<div class="date">
    <h4>start</h4>
    <h2></h2>
    <h3></h3>
    <div style="text-align: center;">
        <input type="hidden" class="getit">
    </div>
</div>

<div class="date">
    <h4>end</h4>
    <h2></h2>
    <h3></h3>
    <div style="text-align: center;">
        <input type="hidden" class="getit">
    </div>
</div>

<script type="text/javascript">
    for (var i = 0; i < document.getElementsByClassName('getit').length; i++){
        popDate(document.getElementsByClassName('getit')[i], {
            format: 'the {D} of {MMMMM}',
            onChoose: function (day) {
            	var  c = this.parentElement.parentElement;
            	c.querySelector('h2').innerHTML = day.dd;
            	c.querySelector('h3').innerHTML = 'of ' + day.mmmm;
            	c.setAttribute('title', day.format);
            }
        });    
    }
</script>
```

## Contributors
Write me: [eat.yummies@gmail.com](mailto:eat.yummies@gmail.com)
Fork or download on [GitLab](https://gitlab.com/jokernakartama/popdate) 


## License

MIT